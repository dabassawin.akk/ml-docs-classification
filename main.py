from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, UploadFile
from keras.models import load_model
from tensorflow import nn
import numpy as np
from PIL import Image
from io import BytesIO

model = load_model("trained_model_3_classes.h5")

app = FastAPI()

app.add_middleware(
  CORSMiddleware,
  allow_origins=["*"],
  allow_methods=["*"],
  allow_headers=["*"]
)

classNames = [
  {
    "name": "book_bank",
    "nameTH": "สมุดบัญชีหน้าแรก"
  },
  {
    "name": "driving_license",
    "nameTH": "ใบขับขี่"
  },
  {
    "name": "id_card",
    "nameTH": "บัตรประชาชน"
  }
]

def read_image(image_encoded):
  im = bytearray(image_encoded)
  pil_image = Image.open(BytesIO(im))
  return pil_image.resize((256, 256))

@app.get("/")
async def root():
  return {"status": "ok"}

@app.get("/api/class-names")
async def getClassNames():
  return classNames

@app.post("/api/prediction")
async def predictImages(files: list[UploadFile]):
  response = []
  for file in files:
    img = read_image(await file.read())
    imgArray = np.asarray(img)
    imgArray = np.expand_dims(imgArray, axis=0)
    images = np.vstack([imgArray])
    predictions = model.predict(images)
    response.append({
      "id": file.filename,
      "className": classNames[np.argmax(predictions)]["name"],
      "classNameTH": classNames[np.argmax(predictions)]["nameTH"],
      "score": np.max(predictions) * 100
    })
  return response
