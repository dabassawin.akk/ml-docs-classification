FROM python:3.10.9
ENV PORT=$PORT
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE $PORT
CMD uvicorn main:app --host 0.0.0.0 --port $PORT